﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DonutTest
{
    [CreateAssetMenu(fileName = "ViewModelLibrary", menuName = "ScriptableObjects/ViewModelLibrary")]
    public class ViewModelLibrary : ScriptableObject, IViewModelLibrary
    {
        // TODO? Find view models in some folder instead of UpdateData?
        [SerializeField] private ViewModelBase[] _viewModels;

        // Can be cached and updated in editor before build or after adding new viewModels
        private Dictionary<Type, ViewModelBase> _viewModelsByType;

        public void Init()
        {
            UpdateData();
        }

        public T GetPrefab<T>() where T : ViewModelBase
        {
            Type type = typeof(T);
            if (!_viewModelsByType.ContainsKey(type))
            {
                Debug.LogError($"Window with type {typeof(T)} not found");
                return null;
            }

            return _viewModelsByType[type] as T;
        }

        private void UpdateData()
        {
            _viewModelsByType = new Dictionary<Type, ViewModelBase>();
            foreach (ViewModelBase viewModel in _viewModels)
            {
                Type type = viewModel.GetComponent<ViewModelBase>().GetType();
                _viewModelsByType.Add(type, viewModel);
            }
        }
    }
}