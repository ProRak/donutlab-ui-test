﻿using UnityEngine;

namespace DonutTest
{
    public class UIManager : IUIManager
    {
        private readonly IViewModelLibrary _windowLibrary;
        private readonly Canvas _rootCanvas;

        private GameObject _activeWindow;

        public UIManager(IViewModelLibrary windowLibrary, Canvas rootCanvas)
        {
            _windowLibrary = windowLibrary;
            _rootCanvas = rootCanvas;
        }

        public void Show<T, TM>(TM model) where T : UnityWeldViewModelBase<TM>
        {
            // No need cache now
            if (_activeWindow != null)
                Object.Destroy(_activeWindow);

            T prefab = _windowLibrary.GetPrefab<T>();
            T newWindow = Object.Instantiate(prefab, _rootCanvas.transform).GetComponent<T>();
            newWindow.Init(model);
            _activeWindow = newWindow.gameObject;
        }
    }
}