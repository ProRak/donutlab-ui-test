﻿using System.ComponentModel;

namespace DonutTest
{
    public abstract class UnityWeldViewModelBase<T> : ViewModelBase, INotifyPropertyChanged
    {
        public abstract void Init(T model);

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}