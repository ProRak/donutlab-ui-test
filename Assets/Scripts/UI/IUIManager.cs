﻿namespace DonutTest
{
    public interface IUIManager
    {
        void Show<T, TM>(TM model) where T : UnityWeldViewModelBase<TM>;
    }
}