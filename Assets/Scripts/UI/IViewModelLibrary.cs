﻿namespace DonutTest
{
    public interface IViewModelLibrary
    {
        T GetPrefab<T>() where T : ViewModelBase;
    }
}