﻿using UnityEngine;
using UnityEngine.UI;

namespace DonutTest
{
    public class TabColorBasedOnActiveTab : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private TabType _tabType;

        private TabType _activeTab;

        public TabType ActiveTab
        {
            get => _activeTab;
            set
            {
                _activeTab = value;
                _image.enabled = _activeTab != _tabType;
            }
        }
    }
}