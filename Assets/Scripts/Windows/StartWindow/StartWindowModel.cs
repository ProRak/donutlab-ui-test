﻿using Zenject;

namespace DonutTest
{
    public class StartWindowModel : IStartWindowModel
    {
        private readonly IUIManager _uiManager;
        private readonly WindowWithTextsModelFactory _windowWithTextsModelFactory;

        public StartWindowModel(IUIManager uiManager, WindowWithTextsModelFactory windowWithTextsModelFactory)
        {
            _uiManager = uiManager;
            _windowWithTextsModelFactory = windowWithTextsModelFactory;
        }

        public void ShowWindowWithTexts()
        {
            IWindowWithTextsModel model = _windowWithTextsModelFactory.Create();
            _uiManager.Show<WindowWithTextsViewModel, IWindowWithTextsModel>(model);
        }

        public class Factory : PlaceholderFactory<IStartWindowModel> { }
    }
}