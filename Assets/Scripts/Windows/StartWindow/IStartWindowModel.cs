﻿using Zenject;

namespace DonutTest
{
    public interface IStartWindowModel
    {
        void ShowWindowWithTexts();
    }

    public class StartWindowModelFactory : PlaceholderFactory<IStartWindowModel>
    {
    }
}