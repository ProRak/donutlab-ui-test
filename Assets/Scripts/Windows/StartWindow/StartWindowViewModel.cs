﻿using UnityWeld.Binding;

namespace DonutTest
{
    [Binding]
    public class StartWindowViewModel : UnityWeldViewModelBase<IStartWindowModel>
    {
        private IStartWindowModel _model;

        public override void Init(IStartWindowModel model)
        {
            _model = model;
        }

        [Binding]
        public void StartButtonClick()
        {
            _model.ShowWindowWithTexts();
        }
    }
}