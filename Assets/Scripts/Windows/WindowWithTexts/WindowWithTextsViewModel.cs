﻿using UniRx;
using UnityWeld.Binding;

namespace DonutTest
{
    [Binding]
    public class WindowWithTextsViewModel : UnityWeldViewModelBase<IWindowWithTextsModel>
    {
        private IWindowWithTextsModel _model;
        private string _text = string.Empty;
        private TabType _activeTabType = TabType.None;

        public override void Init(IWindowWithTextsModel model)
        {
            _model = model;

            _model.TextOnActiveTab.Subscribe(t => Text = t).AddTo(this);
            _model.ActiveTabType.Subscribe(t => ActiveTabType = t).AddTo(this);
        }

        [Binding]
        public string Text
        {
            get => _text;
            private set
            {
                if (_text == value)
                {
                    return;
                }

                _text = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        [Binding]
        public TabType ActiveTabType
        {
            get => _activeTabType;
            private set
            {
                if (_activeTabType == value)
                {
                    return;
                }

                _activeTabType = value;
                OnPropertyChanged(nameof(ActiveTabType));
            }
        }

        [Binding]
        public void ChangeTextButtonClick()
        {
            _model.ChangeTextOnActiveTab();
        }

        [Binding]
        public void SelectFirstTab()
        {
            _model.SetActiveTab(TabType.First);
        }

        [Binding]
        public void SelectSecondTab()
        {
            _model.SetActiveTab(TabType.Second);
        }

        [Binding]
        public void CloseWindow()
        {
            _model.CloseWindow();
        }
    }
}