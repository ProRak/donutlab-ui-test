﻿using System;
using System.Collections.Generic;
using UniRx;

namespace DonutTest
{
    public class WindowWithTextsModel : IWindowWithTextsModel
    {
        private readonly ITextProvider _textProvider;
        private readonly IUIManager _uiManager;
        private readonly StartWindowModelFactory _startWindowModelFactory;

        private readonly Dictionary<TabType, string> _textOnTabs;

        public WindowWithTextsModel(ITextProvider textProvider, IUIManager uiManager, StartWindowModelFactory startWindowModelFactory)
        {
            _textProvider = textProvider;
            _uiManager = uiManager;
            _startWindowModelFactory = startWindowModelFactory;

            _textOnTabs = new Dictionary<TabType, string>();
            foreach (TabType tabType in Enum.GetValues(typeof(TabType)))
            {
                _textOnTabs[tabType] = _textProvider.GenerateRandomText();
            }

            TextOnActiveTab = new ReactiveProperty<string>();
            ActiveTabType = new ReactiveProperty<TabType>(TabType.First);

            ActualizeTextOnActiveTab();
        }

        public ReactiveProperty<string> TextOnActiveTab { get; }
        public ReactiveProperty<TabType> ActiveTabType { get; }

        public void SetActiveTab(TabType newActiveTab)
        {
            ActiveTabType.Value = newActiveTab;
            ActualizeTextOnActiveTab();
        }

        public void ChangeTextOnActiveTab()
        {
            _textOnTabs[ActiveTabType.Value] = _textProvider.GenerateRandomText();
            ActualizeTextOnActiveTab();
        }

        public void CloseWindow()
        {
            IStartWindowModel model = _startWindowModelFactory.Create();
            _uiManager.Show<StartWindowViewModel, IStartWindowModel>(model);
        }

        private void ActualizeTextOnActiveTab()
        {
            if (ActiveTabType.Value == TabType.None)
            {
                TextOnActiveTab.Value = string.Empty;
                return;
            }

            TextOnActiveTab.Value = _textOnTabs[ActiveTabType.Value];
        }
    }
}