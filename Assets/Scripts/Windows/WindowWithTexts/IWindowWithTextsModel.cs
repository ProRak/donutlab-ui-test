﻿using UniRx;
using Zenject;

namespace DonutTest
{
    public interface IWindowWithTextsModel
    {
        ReactiveProperty<string> TextOnActiveTab { get;}
        ReactiveProperty<TabType> ActiveTabType { get;}

        void SetActiveTab(TabType newActiveTab);
        void ChangeTextOnActiveTab();
        void CloseWindow();
    }

    public class WindowWithTextsModelFactory : PlaceholderFactory<IWindowWithTextsModel>
    {
    }
}