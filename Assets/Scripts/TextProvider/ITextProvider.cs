﻿namespace DonutTest
{
    public interface ITextProvider
    {
        string GenerateRandomText();
    }
}