﻿using System;
using UnityEngine;
using Zenject;

namespace DonutTest
{
    public class GuidTextProvider : ITextProvider
    {
        public string GenerateRandomText()
        {
            return Guid.NewGuid().ToString();
        }
    }
}