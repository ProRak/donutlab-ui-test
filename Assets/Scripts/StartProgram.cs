﻿using UnityEngine;
using Zenject;

namespace DonutTest
{
    // Using for show first UI window
    public class StartProgram : MonoBehaviour
    {
        private StartWindowModelFactory _startWindowModelFactory;
        private IUIManager _uiManager;

        [Inject]
        public void Init(IUIManager uiManager, StartWindowModelFactory startWindowModelFactory)
        {
            _uiManager = uiManager;
            _startWindowModelFactory = startWindowModelFactory;
        }

        private void Start()
        {
            IStartWindowModel model = _startWindowModelFactory.Create();
            _uiManager.Show<StartWindowViewModel, IStartWindowModel>(model);

            Destroy(gameObject);
        }
    }
}