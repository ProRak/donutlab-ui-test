﻿using UnityEngine;
using Zenject;

namespace DonutTest
{
    public class GameInstaller : MonoInstaller<GameInstaller>
    {
        [SerializeField] private ViewModelLibrary _viewModelLibrary;
        [SerializeField] private Canvas _rootCanvas;

        public override void InstallBindings()
        {
            Container.Bind<ITextProvider>().To<GuidTextProvider>().AsSingle();
            Container.Bind<Canvas>().FromInstance(_rootCanvas).AsSingle();

            Container.BindFactory<IStartWindowModel, StartWindowModelFactory>().To<StartWindowModel>();
            Container.BindFactory<IWindowWithTextsModel, WindowWithTextsModelFactory>().To<WindowWithTextsModel>();

            Container.Bind<IUIManager>().To<UIManager>().AsSingle();
            Container.Bind<IViewModelLibrary>().To<ViewModelLibrary>().FromInstance(_viewModelLibrary).AsSingle()
                .OnInstantiated<ViewModelLibrary>((context, instance) => instance.Init());
        }
    }
}